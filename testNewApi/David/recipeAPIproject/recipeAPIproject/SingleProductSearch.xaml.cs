﻿using System;
using System.Collections.ObjectModel;
using System.Net.Http;
//using ModernHttpClient;
using recipeAPIproject.Models;
using Xamarin.Forms;

namespace recipeAPIproject
{
    public partial class SingleProductSearch : ContentPage
    {
        public SingleProductSearch()
        {
            InitializeComponent();
        }

        public SingleProductSearch(string query, string category)
        {
            InitializeComponent();
            Header.Text = "Search Results for " + query;
            GetSearchResults(query, category);
        }

        async void GetSearchResults(string query, string category)
        {
            HttpClient client1 = new HttpClient();
            var uri = new Uri(
                string.Format(
                    $"https://api.walmartlabs.com/v1/search?query=" +
                    $"{query.ToLower()}" +
                    $"&format=json&numItems=25&categoryId={category}&apiKey=uxzftrq4jc4ft8arpwtaxr4x"));
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;
            request.Headers.Add("Application", "application / json");
            HttpResponseMessage response = await client1.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                WalmartSearchQuery results = WalmartSearchQuery.FromJson(content);
                var resultList = new ObservableCollection<Item>(results.Items);
                SearchResults.ItemsSource = resultList;
            }
        }

        void Handle_ItemTapped(object sender, System.EventArgs e)
        {
            var listView = (ListView)sender;
            Item productChosen = (Item)listView.SelectedItem;
            var uri = new Uri(productChosen.ProductUrl);
            Device.OpenUri(uri);
        }
    }
}
