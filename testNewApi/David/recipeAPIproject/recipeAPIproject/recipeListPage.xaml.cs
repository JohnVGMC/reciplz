﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Plugin.Connectivity;
using recipeAPIproject.Models;
using Xamarin.Forms;
using System.Net.Http;

namespace recipeAPIproject
{
    public partial class recipeListPage : ContentPage
    {
        public recipeListPage()
        {
            InitializeComponent();
        }

        public recipeListPage(List<string> apiCall )
        {
            InitializeComponent();
            if(CheckConnection())
                MakeCall(apiCall);
            else
                DisplayAlert("WARNING", "You are not on the line!", "OK");
        }

        private bool CheckConnection()
        {
            bool internetStatus;
            if (CrossConnectivity.Current.IsConnected == true)
                internetStatus = true;
            else
                internetStatus = false;
            return internetStatus;
        }

       async void MakeCall(List<string> apiCall)
        {
            HttpClient client = new HttpClient();
            var uri = new Uri(string.Format("https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/findByIngredients?number=70&ranking=2&ingredients="+$"{string.Join("%2C", apiCall)}"+"&fillIngredients=false"));
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;
            request.Headers.Add("X-Mashape-Key", "Z3BpKqGIcamsh2j4hy731CSFZMoap10yboUjsnvWMsONHp8aZE");
            //request.Headers.Add("X-Mashape-Host", Keys.mashapeHost);
            request.Headers.Add("Application", "application / json");
            List<recipeNew> recresponse = null;
            HttpResponseMessage response = await client.SendAsync(request);
            newlabel.Text = response.IsSuccessStatusCode.ToString();
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                recresponse = recipeNew.FromJson(content); 
                newlabel.Text = "We found "  +recresponse.Count().ToString()+ " recipes!";
                PopulateRecipeList(recresponse);
            }
        }

        void PopulateRecipeList(List<recipeNew> recipes)
        {
            var listOfRecipes = new ObservableCollection<recipeNew>(recipes as List<recipeNew>);
            RecipeListView.ItemsSource = listOfRecipes;
        }

        async void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var item = (ListView)sender;
            recipeNew itemTapped = (recipeNew)item.SelectedItem;
            await Navigation.PushAsync(new ingredientPage(itemTapped));
        }
        void Handle_Disappearing(object sender, System.EventArgs e)
        {
            // throw new NotImplementedException();
        }

        void Handle_Appearing(object sender, System.EventArgs e)
        {
            // throw new NotImplementedException();
        }
    }
}
