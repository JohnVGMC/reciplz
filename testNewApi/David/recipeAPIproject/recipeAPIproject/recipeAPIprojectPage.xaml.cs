﻿using Xamarin.Forms;
using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using recipeAPIproject.Models;


namespace recipeAPIproject
{
    public partial class recipeAPIprojectPage : ContentPage
    {
        List<string> iList = new List<string>();
        public recipeAPIprojectPage()
        {
            InitializeComponent();
            PopulateIngredientList();
        }
        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
           var item = (ListView)sender;
           Ingredient itemTapped = (Ingredient)item.SelectedItem;
           if(itemTapped.detail == "Lose it!")//use it
            {
                if(itemTapped.count == 0)
                {
                    itemTapped.count++;
                    iList.Add(itemTapped.ingredientName);
                }
                else if(itemTapped.count == 1)
                {
                    iList.Remove(itemTapped.ingredientName);
                    itemTapped.count--;
                }
                else{};
                finalOrder.Text = "Cooking with : " + string.Join(",", iList);
            }
           else//dont use it
              itemTapped.detail = "Cook it!";
              //find way to change color
            
        }
        public void PopulateIngredientList()
        {
            var ingrList = new ObservableCollection<Ingredient>()
            {
                new Ingredient()
                {
                    ingredientName="Butter",
                    imgUrl= ImageSource.FromFile("Butter.jpg"),
                    urlExtension = "butter",
                    detail = "Lose it!",
                    selected=false,
                    count =0,
                },
                new Ingredient()
                {
                    ingredientName="Yogurt",
                    imgUrl= ImageSource.FromFile("gyogurt.png"),
                    urlExtension = "yogurt",
                    detail = "Lose it!",
                    selected=false,
                    count =0,
                },
                new Ingredient()
                {
                    ingredientName="Eggs",
                    imgUrl= ImageSource.FromFile("eggs.png"),
                    urlExtension = "eggs",
                    detail = "Lose it!",
                    selected=false,
                    count =0,
                },
                new Ingredient()
                {
                    ingredientName="Garlic",
                    imgUrl= ImageSource.FromFile("garlic.png"),
                    urlExtension = "garlic",
                    detail = "Lose it!",
                    selected=false,
                    count =0,
                },
                new Ingredient()
                {
                    ingredientName="Tomato",
                    imgUrl= ImageSource.FromFile("tomato.png"),
                    urlExtension = "tomato",
                    detail = "Lose it!",
                    selected=false,
                    count =0,
                },
                new Ingredient()
                {
                    ingredientName="Beans",
                    imgUrl= ImageSource.FromFile("beans.png"),
                    urlExtension = "beans",
                    detail = "Lose it!",
                    selected=false,
                    count =0,
                },
                new Ingredient()
                {
                    ingredientName="Chicken",
                    imgUrl= ImageSource.FromFile("chicken.png"),
                    urlExtension = "chicken",
                    detail = "Lose it!",
                    selected=false,
                    count =0,
                },
                new Ingredient()
                {
                    ingredientName="Turkey",
                    imgUrl= ImageSource.FromFile("turkey.png"),
                    urlExtension = "turkey",
                    detail = "Lose it!",
                    selected=false,
                    count =0,
                },
                new Ingredient()
                {
                    ingredientName="Beef",
                    imgUrl= ImageSource.FromFile("beef.png"),
                    urlExtension = "beef",
                    detail = "Lose it!",
                    selected=false,
                    count =0,
                },
            };
            IngredientListView.ItemsSource = ingrList;
        }

        void Handle_ClickedSubmitForm(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new recipeListPage(iList));
        }

        void Handle_ClickedResetForm(object sender, System.EventArgs e)
        {
            iList.Clear();
            finalOrder.Text = "Cooking with : " + string.Join(", ", iList);
        }
        void Handle_Disappearing(object sender, System.EventArgs e)
        {
            // throw new NotImplementedException();
        }

        void Handle_Appearing(object sender, System.EventArgs e)
        {
            var navigationPage = new NavigationPage(new recipeAPIprojectPage());// throw new NotImplementedException();
        }
    }
}
