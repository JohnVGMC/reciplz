﻿namespace recipeAPIproject.Models
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using Xamarin.Forms;
    using System.Net.Http;
    using System.Text;
    public partial class user
    {
        [JsonProperty("_id")]
        public string id { get; set; }

        [JsonProperty("password")]
        public string password { get; set; }

        [JsonProperty("email")]
        public string email { get; set; }

        [JsonProperty("_mock")]
        public bool mock { get; set; }
    }
    public partial class savedrecipe
    {
        [JsonProperty("_id")]
        public string id { get; set; }

        [JsonProperty("publisher")]
        public string publisher { get; set; }

        [JsonProperty("f2f_url")]
        public string f2f_url { get; set; }

        [JsonProperty("title")]
        public string title { get; set; }

        [JsonProperty("recipe_id")]
        public string recipe_id { get; set; }

        [JsonProperty("source_url")]
        public string source_url { get; set; }

        [JsonProperty("image_url")]
        public string image_url { get; set; }

        [JsonProperty("social_rank")]
        public double social_rank;

        [JsonProperty("publisher_url")]
        public string publisher_url { get; set; }

        [JsonProperty("user_id")]
        public string user_id;

        [JsonProperty("_mock")]
        public bool mock { get; set; }
    }
    public partial class user
    {
        public static List<user> FromJson(string json) => JsonConvert.DeserializeObject<List<user>>(json);
    }

    public partial class savedrecipe
    {
        public static List<savedrecipe> FromJson(string json) => JsonConvert.DeserializeObject<List<savedrecipe>>(json);
    }
    public static class Extensions
    {
        public static StringContent AsJson(this object o)
         => new StringContent(JsonConvert.SerializeObject(o), Encoding.UTF8, "application/json");
    }

    public static class Serialize1
    {
        public static string ToJson(this user self) => JsonConvert.SerializeObject(self, recipeAPIproject.Models.Converter.Settings);
        //public static string ToJson(this recipe self) => JsonConvert.SerializeObject(self, recipeAPIproject.Models.Converter.Settings);

    }

    internal class Converter1
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = { },
        };
    }
}
