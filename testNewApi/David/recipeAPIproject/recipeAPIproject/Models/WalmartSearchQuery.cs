﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using walmartItemTest.Models;
//
//    var walmartSearchQuery = WalmartSearchQuery.FromJson(jsonString);

namespace recipeAPIproject.Models
{
    using System;
    using System.Collections.Generic;
    using Xamarin.Forms;
    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class WalmartSearchQuery
    {
        [JsonProperty("query")]
        public string Query { get; set; }

        [JsonProperty("sort")]
        public string Sort { get; set; }

        [JsonProperty("responseGroup")]
        public string ResponseGroup { get; set; }

        [JsonProperty("totalResults")]
        public long TotalResults { get; set; }

        [JsonProperty("start")]
        public long Start { get; set; }

        [JsonProperty("numItems")]
        public long NumItems { get; set; }

        [JsonProperty("items")]
        public List<Item> Items { get; set; }
    }

    public partial class Item
    {
        [JsonProperty("itemId")]
        public long ItemId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("salePrice")]
        public double SalePrice { get; set; }

        [JsonProperty("thumbnailImage")]
        public string ThumbnailImage { get; set; }

        [JsonProperty("productUrl")]
        public string ProductUrl { get; set; }

        [JsonProperty("customerRating", NullValueHandling = NullValueHandling.Ignore)]
        public string CustomerRating { get; set; }

        [JsonProperty("customerRatingImage", NullValueHandling = NullValueHandling.Ignore)]
        public string CustomerRatingImage { get; set; }

        [JsonProperty("addToCartUrl")]
        public string AddToCartUrl { get; set; }

        // for use only in ProductSuggestions
        public string IngredientQuery { get; set; }
        public string SearchCategory { get; set; }
    }

    public partial class WalmartSearchQuery
    {
        public static WalmartSearchQuery FromJson(string json) => JsonConvert.DeserializeObject<WalmartSearchQuery>(json, recipeAPIproject.Models.Converter.Settings);
    }

    public static class Serialize1
    {
        public static string ToJson(this WalmartSearchQuery self) => JsonConvert.SerializeObject(self, recipeAPIproject.Models.Converter.Settings);
    }

    internal static class Converter1
    {

        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
