﻿using System;
using Xamarin.Forms;

namespace recipeAPIproject.Models
{
    public class IngredientCategory
    {
        public string Name
        { get; set; }

        public string WalmartCategory
        { get; set; }
    }
}
