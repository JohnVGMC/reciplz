﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace recipeAPIproject.Models
{
    public class Ingredient
    {
        public string ingredientName
        { get; set; }

        public ImageSource imgUrl
        { get; set; }

        public string detail
        { get; set; }

        public string urlExtension
        { get; set; }

        public bool selected
        { get; set; }

        public int count
        { get; set; }
    }
   
}