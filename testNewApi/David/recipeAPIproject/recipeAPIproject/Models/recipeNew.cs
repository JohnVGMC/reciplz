﻿namespace recipeAPIproject.Models
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using Xamarin.Forms;

    //individual recipe item
    public partial class recipeNew
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("title")]
        public string title { get; set; }

        [JsonProperty("image")]
        public ImageSource image { get; set; }

        [JsonProperty("imageType")]
        public string imageType { get; set; }

        [JsonProperty("usedIngredientCount")]
        public int usedIngredientCount { get; set; }

        [JsonProperty("missedIngredientCount")]
        public int missedIngredientCount { get; set; }

        [JsonProperty("likes")]
        public int likes { get; set; }
    }

    //expected returned list of recipes

    public partial class recipeNew
    {
        public static List<recipeNew> FromJson(string json) => JsonConvert.DeserializeObject<List<recipeNew>>(json);
    }

    public static class Serialize
    {
        public static string ToJson(this recipeNew self) => JsonConvert.SerializeObject(self, recipeAPIproject.Models.Converter.Settings);
    }

    internal class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = { },
        };
    }
}