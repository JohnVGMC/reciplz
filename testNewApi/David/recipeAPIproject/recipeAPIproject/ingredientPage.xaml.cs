﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Plugin.Connectivity;
using recipeAPIproject.Models;
using Xamarin.Forms;
using System.Net.Http;


namespace recipeAPIproject
{
   
    public partial class ingredientPage : ContentPage
    {
        List<string> ingredients = new List<string>();
        public string link;
        public ingredientPage()
        {
            InitializeComponent();
        }
        public ingredientPage(recipeNew recipe)
        {
            InitializeComponent();
            MakeRecipeCall(recipe);
        }

        async void MakeRecipeCall(recipeNew recipe)
        {
            string template = "https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/{0}/information";
            string changes = recipe.id.ToString();
            var uri = new Uri(string.Format(template, changes));// + recipe.id.ToString()));
            HttpClient client = new HttpClient();
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;
            request.Headers.Add("X-Mashape-Key", "Z3BpKqGIcamsh2j4hy731CSFZMoap10yboUjsnvWMsONHp8aZE");
            //request.Headers.Add("X-Mashape-Host", Keys.mashapeHost);
            request.Headers.Add("Application", "application / json");
            HttpResponseMessage response = await client.SendAsync(request);
            RecipeInfo recInfo = null;
            recipeName.Text = response.IsSuccessStatusCode.ToString();
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                recInfo = RecipeInfo.FromJson(content);
                recipeImage.Source = recInfo.Image;
                recipeName.Text = recInfo.Title;
                recipeTime.Text = recipeTime.Text + recInfo.ReadyInMinutes + " minutes.";
                recipeServes.Text = recipeServes.Text + recInfo.Servings;
                directions.Text = recInfo.Instructions;
                link = recInfo.SourceUrl;

                for (int i = 0; i < recInfo.ExtendedIngredients.Count()-1; i++)
                {
                    ingredients.Add(recInfo.ExtendedIngredients[i].Name);
                }
            }
        }
            async void Handle_ClickedLink(object sender, System.EventArgs e)
            {
                await DisplayAlert("Leaving ReciPLZ", "Don't worry we'll be right here when you come back.", "Got it");
                var uri = new Uri(link);
                Device.OpenUri(uri);
            }

            void Handle_Clicked(object sender, System.EventArgs e)
            {
                Navigation.PushAsync(new ProductSuggestions(ingredients));
            }

        }
    }

