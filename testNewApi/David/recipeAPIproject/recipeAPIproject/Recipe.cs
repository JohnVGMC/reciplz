﻿//using SQLitePCL;
using System.ComponentModel;
using SQLite;

namespace recipeAPIproject
{
    [Table("Recipes")]
    public class Recipe : INotifyPropertyChanged
    {
        private int _id;
        [PrimaryKey]
        public int id
        {
            get
            {
                return _id;
            }
            set
            {
                this._id = value;
                OnPropertyChanged(nameof(id));
            }
        }
        private string _publisher;
        public string publisher
        {
            get
            {
                return _publisher;
            }
            set
            {
                this._publisher = value;
                OnPropertyChanged(nameof(publisher));
            }
        }
        private string _f2f_url;
        public string f2f_url
        {
            get
            {
                return _f2f_url;
            }
            set
            {
                this._f2f_url = value;
                OnPropertyChanged(nameof(f2f_url));
            }
        }
        private string _title;
        public string title
        {
            get
            {
                return _title;
            }
            set
            {
                this._title = value;
                OnPropertyChanged(nameof(title));
            }
        }
        private string _source_url;
        public string source_url
        {
            get
            {
                return _source_url;
            }
            set
            {
                this._source_url = value;
                OnPropertyChanged(nameof(source_url));
            }
        }
        private string _image_url;
        public string image_url
        {
            get
            {
                return _image_url;
            }
            set
            {
                this._image_url = value;
                OnPropertyChanged(nameof(image_url));
            }
        }
        private string _social_rank;
        public string social_rank
        {
            get
            {
                return _social_rank;
            }
            set
            {
                this._social_rank = value;
                OnPropertyChanged(nameof(social_rank));
            }
        }
        private string _publisher_url;
        public string publisher_url
        {
            get
            {
                return _publisher_url;
            }
            set
            {
                this._publisher_url = value;
                OnPropertyChanged(nameof(publisher_url));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this,
              new PropertyChangedEventArgs(propertyName));
        }
    }
}