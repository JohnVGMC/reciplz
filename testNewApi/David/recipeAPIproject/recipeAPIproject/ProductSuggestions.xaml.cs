﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Net.Http;
//using ModernHttpClient;
using recipeAPIproject.Models;
using Xamarin.Forms;

namespace recipeAPIproject
{
    public partial class ProductSuggestions : ContentPage
    {
        public List<IngredientCategory> searchConstraints;

        public ProductSuggestions()
        {
            InitializeComponent();
            SetSearchConstraints();
        }

        public ProductSuggestions(List<string> ingredients)
        {
            InitializeComponent();
            SetSearchConstraints();
            GetTopResults(ingredients); 
        }

        // Due to the Walmart's API limit of 5 calls per second
        // The top result of each ingredient is limited to 5 of the first ingredients of the array
        async void GetTopResults(List<string> ingredientList)
        {
            var productSuggestions = new ObservableCollection<Item>();
            string categoryConstraint;
            for (int i = 0; (i < ingredientList.Count()) && (i < 5); i++)
            {
                int constraintIndex = searchConstraints.FindIndex(item => item.Name == ingredientList[i].ToLower());
                if (constraintIndex >= 0) { categoryConstraint = searchConstraints[constraintIndex].WalmartCategory; }
                else { categoryConstraint = "0"; }
                // Get Web content
                HttpClient client1 = new HttpClient();
                var uri = new Uri(
                    string.Format(
                        $"https://api.walmartlabs.com/v1/search?query=" +
                        $"{ingredientList[i].ToLower()}" +
                        $"&format=json&numItems=1&categoryId={categoryConstraint}&apiKey=uxzftrq4jc4ft8arpwtaxr4x"));
                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;
                request.Headers.Add("Application", "application / json");
                HttpResponseMessage response = await client1.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        WalmartSearchQuery results = WalmartSearchQuery.FromJson(content);
                        results.Items[0].IngredientQuery = ingredientList[i].ToUpper();
                        results.Items[0].SearchCategory = categoryConstraint;
                        productSuggestions.Add(results.Items[0]);
                    }
                    catch(NullReferenceException){
                        // Just move on to the next item query and don't bother trying to add to the product suggestions list
                    }
                }
            }
            SearchResults.ItemsSource = productSuggestions;
        }

        async void Handle_ItemTapped(object sender, System.EventArgs e)
        {
            var listView = (ListView)sender;
            Item suggestionChosen = (Item)listView.SelectedItem;
            var action = await DisplayActionSheet("View Product Details?", "Cancel", null, "View Product", "See More Suggestions");
            switch(action.ToString()){
                case "View Product":
                    var uri = new Uri(suggestionChosen.ProductUrl);
                    Device.OpenUri(uri);
                    break;
                case "See More Suggestions":
                    await Navigation.PushAsync(new SingleProductSearch(suggestionChosen.IngredientQuery, suggestionChosen.SearchCategory)); 
                    break;
            }
        }

        private void SetSearchConstraints()
        {
            searchConstraints = new List<IngredientCategory>()
            {
                new IngredientCategory()
                {
                    Name = "beans",
                    WalmartCategory = "976759_976794_1001478",
                },
                new IngredientCategory()
                {
                    Name = "butter",
                    WalmartCategory = "976759_976780",
                },
                new IngredientCategory()
                {
                    Name = "turkey",
                    WalmartCategory = "976759_1071964_976789",
                },
                new IngredientCategory()
                {
                    Name = "lettuce",
                    WalmartCategory = "976759_1071964_976793",
                },
            };
        }
    }
}
