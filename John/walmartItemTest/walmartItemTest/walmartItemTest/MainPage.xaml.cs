﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Net.Http;
using ModernHttpClient;
using walmartItemTest.Models;
using Xamarin.Forms;

namespace walmartItemTest
{
    public partial class MainPage : ContentPage
    {

        public MainPage()
        {
            InitializeComponent();
            GoToProductSuggestions();
        }

        void GoToProductSuggestions()
        {
            String[] ingredients = { "beef", "olive oil", "lettuce" };
            Navigation.PushAsync(new ProductSuggestions(ingredients));
        }


        async void Handle_ProductIDEntered(object sender, System.EventArgs e)
        {
            String categoryID = "0";
            var client1 = new HttpClient(new NativeMessageHandler());
            var uri = new Uri(
                string.Format(
                    $"https://api.walmartlabs.com/v1/search?query=" +
                    $"{ProductEntry.Text.ToLower()}" +
                    $"&format=json&numItems=25&categoryId={categoryID}&apiKey=uxzftrq4jc4ft8arpwtaxr4x"));
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;
            request.Headers.Add("Application", "application / json");
            HttpResponseMessage response = await client1.SendAsync(request);
            if(response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                WalmartSearchQuery results = WalmartSearchQuery.FromJson(content);
                Console.Write("Got to here...");
                var resultList = new ObservableCollection<Item>(results.Items);
                SearchResults.ItemsSource = resultList;
            }
            else
            {
                await DisplayAlert("Error", "An error has occurred.", "OK");
            }
        }

    }
}
