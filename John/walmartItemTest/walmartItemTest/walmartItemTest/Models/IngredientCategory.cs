﻿using System;
using Xamarin.Forms;

namespace walmartItemTest.Models
{
    public class IngredientCategory
    {
        public string Name
        { get; set; }

        public string WalmartCategory
        { get; set; }
    }
}
