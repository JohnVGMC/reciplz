﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Plugin.Connectivity;
using recipeAPIproject.Models;
using Xamarin.Forms;
using System.Net.Http;

namespace recipeAPIproject
{
    public partial class recipeListPage : ContentPage
    {
        public recipeListPage()
        {
            InitializeComponent();
        }

        public recipeListPage(List<string> apiCall )
        {
            InitializeComponent();
            if(CheckConnection())
                MakeCall(apiCall);
            else
                DisplayAlert("WARNING", "You are not on the line!", "OK");
        }

        private bool CheckConnection()
        {
            bool internetStatus;
            if (CrossConnectivity.Current.IsConnected == true)
                internetStatus = true;
            else
                internetStatus = false;
            return internetStatus;
        }

       async void MakeCall(List<string> apiCall)
        {
            HttpClient client = new HttpClient();
            var uri = new Uri(string.Format($"http://food2fork.com/api/search/?key="+$"{Keys.F2FAPIKey}"+$"&q=" + $"{string.Join("&",apiCall)}"));
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;
            request.Headers.Add("Application", "application / json");
            recipeapItem reciperesponse = null;
            HttpResponseMessage response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            { 
                string content = await response.Content.ReadAsStringAsync();
                reciperesponse = recipeapItem.FromJson(content);
                Type.Text = "We found " + Convert.ToString(reciperesponse.count) + " recipes!";
                PopulateRecipeList(reciperesponse.recipe);
            }

        }

        void PopulateRecipeList(List<recipe> recipes)
        {
            var listOfRecipes = new ObservableCollection<recipe>(recipes as List<recipe>);
            RecipeListView.ItemsSource = listOfRecipes;
        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            
        }
        void Handle_Disappearing(object sender, System.EventArgs e)
        {
            // throw new NotImplementedException();
        }

        void Handle_Appearing(object sender, System.EventArgs e)
        {
            // throw new NotImplementedException();
        }
    }
}
