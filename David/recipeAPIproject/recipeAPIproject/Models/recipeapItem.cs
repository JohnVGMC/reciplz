﻿namespace recipeAPIproject.Models
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using Xamarin.Forms;

    public class recipe
    {
        [JsonProperty("publisher")]
        public string publisher { get; set; }

        [JsonProperty("f2f_url")]
        public string f2f_url { get; set; }

        [JsonProperty("title")]
        public string title { get; set; }

        [JsonProperty("source_url")]
        public string source_url { get; set; }

        [JsonProperty("recipe_id")]
        public string recipe_id { get; set; }

        [JsonProperty("image_url")]
        public ImageSource image_url { get; set; }

        [JsonProperty("social_rank")]
        public string social_rank { get; set; }

        [JsonProperty("publisher_url")]
        public string publisher_url { get; set; }
    }
    public partial class recipeapItem
    {
        [JsonProperty("count")]
        public int count { get; set; }
        [JsonProperty("recipes")]
        public List<recipe> recipe { get; set; }
    }
    public class users
    {
        public string email { get; set; }
        public string password { get; set; }

        public int id { get; internal set; }
    }
    public partial class recipeapItem
    {
        public static recipeapItem FromJson(string json) => JsonConvert.DeserializeObject<recipeapItem>(json);//, webserviceapi.Models.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this recipeapItem self) => JsonConvert.SerializeObject(self, recipeAPIproject.Models.Converter.Settings);
        public static string ToJson(this users self) => JsonConvert.SerializeObject(self, recipeAPIproject.Models.Converter.Settings);

    }

    internal class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = { },
        };
    }
}
