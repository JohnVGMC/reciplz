﻿using System.Linq;
using SQLite;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace recipeAPIproject
{
    public class RecipesDataAccess
    {
        private SQLiteConnection database;
        private static object collLock = new object();
        public ObservableCollection<Recipe> Recipes { get; set; }
        public RecipesDataAccess()
        {
            database =
              DependencyService.Get<IDatabaseConnection>().
              DbConnection();
            database.CreateTable<Recipe>();
            this.Recipes =
              new ObservableCollection<Recipe>(database.Table<Recipe>());
            // If the table is empty, initialize the collection
            if (!database.Table<Recipe>().Any())
            {
                AddNewRecipe();
            }
        }
        public void AddNewRecipe()
        {
            this.Recipes.
              Add(new Recipe
              {
                id = 0,
                publisher="Publisher ...",
                f2f_url="f2f_url ...",
                title="title ...",
                source_url="source_url ...",
                image_url="image_url ...",
                social_rank="social_rank ...",
                publisher_url="publisher_url ..."
              });
        }
        public IEnumerable<Recipe> GetAllRecipes()
        {
            lock(collLock)
            {
                var query = from rec in database.Table<Recipe>()
                            where rec.id != 0
                            select rec;
                return query.AsEnumerable();
            }
        }
        public bool SaveRecipe(Recipe recipeInstance)
        {
            lock (collLock)
            {
                database.Insert(recipeInstance);
            }
            return true;

        }
        public bool DeleteRecipe(Recipe recipeInstance)
        {
            var id = recipeInstance.id;
            lock (collLock)
            {
                database.Delete<Recipe>(recipeInstance);
            }
            this.Recipes.Remove(recipeInstance);
            return true;
        }
        public void SaveAllRecipes()
        {
            lock (collLock)
            {
                foreach (var recipeInstance in this.Recipes)
                {
                    if (recipeInstance.id != 0)
                    {
                        database.Update(recipeInstance);
                    }
                    else
                    {
                        database.Insert(recipeInstance);
                    }
                }
            }
        }
    }
}
