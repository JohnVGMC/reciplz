﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Net.Http;
using recipeAPIproject.Models;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Text;
using System.Collections.ObjectModel;

namespace recipeAPIproject
{
    public partial class savedRecipePage : ContentPage
    {
        public savedRecipePage()
        {
            InitializeComponent();
            requestRecipe();    
        }
        async void requestRecipe()
        {
            HttpClient client = new HttpClient();
            var uri = new Uri(string.Format("https://reciplz-e86f.restdb.io/rest/reciplz-recipes"));
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;
            request.Headers.Add("cache-control", "no-cache");
            request.Headers.Add("x-apikey", "e8b599880e5cdaedb350952463dbba2dc211d");
            request.Headers.Add("Application", "application/json");
            List<savedrecipe> rec = null;
            HttpResponseMessage response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                rec = savedrecipe.FromJson(content);
                savedlabel.Text = "We have found " + (rec.Count) + " stored recipes!";
                PopulateRecipeList(rec);
            }
        }
        void PopulateRecipeList(List<savedrecipe> recipess)
        {
            var listOfRecipes = new ObservableCollection<savedrecipe>(recipess as List<savedrecipe>);
            SavedRecipeListView.ItemsSource = listOfRecipes;
        }

        void Handle_Disappearing(object sender, System.EventArgs e)
        {
            // throw new NotImplementedException();
        }

        void Handle_Appearing(object sender, System.EventArgs e)
        {
            // throw new NotImplementedException();
        }
    }
}
