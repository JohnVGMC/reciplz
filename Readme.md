# ReciPLZ
#### David Moreno Ignacio, John Calderon, Alejandro Aquino
#### CS 481 - Intro To Mobile Programming
#### Professor Thomas Desmond
Visual Studio Project Files for ReciPLZ, our Final Project for CS 481

An APK for Android can be downloaded here to demo: [Download Link](https://drive.google.com/open?id=1JbI3A1pBUOTESM7kaEx0OCx7sBlSHLYe).  
Due to a lack of a paid API key, only half of the original app's functionality is intact to demonstrate.